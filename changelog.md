# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [3.6.0]

## Changed
- Rolling back auth changes.

## [3.5.0]

## Changed
- Updated faker compmoser package to the new fork.

## [3.4.0]

## Changed
- Updated UsersClient to support new SSO auth method when emails have special characters in them.

## [3.3.0] - 2020-06-22

## Changed
- Updated Composer packages including upgrade to PHPUnit 9 and other minor version upgrades.

## [3.2.0] - 2020-05-11

### Changed
- Transporter API `reports/section-assignment-student-reports` and `reports/section-assignment-student-reports-zip` endpoints.

## [3.1.0] - 2019-10-28

### Removed
- Endpoints from the Assignments API client:
    - `/assignments/:status`
    - `/assignments/estimated-hours`
    - `/assignments/:assignmentId/recommended-graiders`

## [3.0.0] - 2019-06-21

## Changed
- Updated Composer packages including upgrade to Carbon v2, PHPUnit 8

## [2.0.0] - 2019-03-20

### Changed
- Updated Composer packages, moved Faker and Mockery out of prod requirements.
- All clients now use the same Username and Password as we've merged our APIs into one.

### Removed
- Testing base class.

## [1.2.1] - 2018-10-24

### Fixed
- Course Integrator did not include `per_page` argument.

## [1.2.0] - 2018-09-12

### Changed
- Updated to PHP 7.2+, PHPUnit 7.0+.

## [1.1.0] - 2018-06-18

### Removed
- Removed `/users` endpoints from the Courses API client.

## [1.0.0] - 2018-03-26

### Fixed
- Codeship build.
- Updating assignment and section-assignment fields.

### Changed
- Updated to PHP 7.1+, PHPUnit 6.0+.
- Added linter

## [0.14.0] - 2017-8-8

### Added
- New rationale enabled field to assignment.

### Fixed
- Code coverage working in container now.

### Removed
- Integration test.

## [0.13.0] - 2017-7-7

### Added
- Transporter API `reports/section-assignment-student-reports` and `reports/section-assignment-student-reports-zip` endpoints.
- Docker for local, Codeship builds

### Fixed
- User `qouta` fields in tests.

## [0.12.0] - 2017-3-27

### Added
- Login endpoint to Users API
- Reset password endpoint to Users API
- Review teacher endpoint to Users API
- Zapier api client
- AcceptanceTester trait to inject mock clients and fake data responses

### Fixed
- Allowing null `$options` parameter in `get<Resource>($id, $options)` calls.

## [0.11.1] - 2017-2-22

### Fixed
- Bug in forgot-password endpoint.

## [0.11.0] - 2017-2-22

### Added
- Integration and unit test suites.
- Forgot password endpoint in Users API.

## [0.10.0] - 2017-1-20

### Added
- Student results export resource to the transport client.

## [0.9.0] - 2017-1-12

### Added
- Students resource to the courses client.
- Ability to retrieve student results by section assignment.
- Ability to attach students to items.

## [0.8.0] - 2016-12-2

### Added
- Rubric user resources to the Rubric client.

## [0.7.0] - 2016-12-1

### Added
- Ability for integrators to replace items themselves with attachments.

## [0.6.0] - 2016-11-28

### Added
- Export teachers and graiders endpoints for Transporter API.


## [0.5.0] - 2016-11-17

### Added
- Import students endpoint for Transporter API.


## [0.4.0] - 2016-11-09

### Added
- Students resource to Courses client.


## [0.3.0] - 2016-11-09

### Added
- Rubrics client

## [0.2.1] - 2016-11-07

### Added
- N/A

### Changed
- Fixed bug in integrators that was missing `per_page` argument.

### Removed
- N/A

## [0.2.0] - 2016-11-03

### Added
- Conversations resource to the Users client

### Changed
- N/A

### Removed
- N/A

## [0.1.0] - 2016-11-03

### Added
- Initial release:
  - Documentation
  - Clients
  - Integrators

### Changed
- N/A

### Removed
- N/A
