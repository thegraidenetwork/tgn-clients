<?php namespace GraideNetwork\Base\Integrators;

abstract class AbstractIntegrator
{
    /**
     * A special value for `itemAttachmentField` that will attach the integrated
     * data to the item itself instead of a defined key
     * @const ATTACH_TO_SELF
     */
    const ATTACH_TO_SELF = '__SELF__';

    /**
     * Default fields for use by callAndAttach method
     * @var array
     */
    protected $defaultFields = [
        'itemAttachmentField' => null,
        'itemForeignKey' => null,
        'attachmentKey' => 'id',
    ];

    /**
     * Attaches an array to another array via foreign key relationships
     *
     * @param array $items
     * @param array $attachments
     * @param string $itemAttachmentField
     * @param string $itemForeignKey
     * @param string $attachmentKey
     *
     * @return array
     */
    public static function attachItems(
        $items = [],
        $attachments = [],
        $itemAttachmentField = null,
        $itemForeignKey = null,
        $attachmentKey = 'id'
    ) {
        if ($attachments) {
            foreach ($items as &$item) {
                foreach ($attachments as $attachment) {
                    if ($item[$itemForeignKey] == $attachment[$attachmentKey]) {
                        if ($itemAttachmentField === self::ATTACH_TO_SELF) {
                            $item = $attachment;
                        } else {
                            $item[$itemAttachmentField] = $attachment;
                        }
                        break;
                    }
                }
            }
        }
        return $items;
    }

    /**
     * Extract specific fields from array of items
     *
     * @param array $items
     * @param null $field
     *
     * @return array
     */
    public static function extractFieldValues($items = [], $field = null)
    {
        return array_map(function ($item) use ($field) {
            return $item[$field];
        }, $items);
    }

    /**
     * Calls the anonymous function and attaches the returned items
     *
     * @param \Closure $attachmentFunction
     * @param array $items
     * @param null $itemAttachmentField
     * @param null $itemForeignKey
     * @param string $attachmentKey
     *
     * @return array
     */
    protected function callAndAttach(\Closure $attachmentFunction, $items = [], $fields = [])
    {
        // Get fields
        $fields = array_merge($this->defaultFields, $fields);

        // Extract the ids
        $ids = $this->extractFieldValues($items, $fields['itemForeignKey']);

        // Attach the returned Items
        return $this->attachItems(
            $items,
            $attachmentFunction($ids),
            $fields['itemAttachmentField'],
            $fields['itemForeignKey'],
            $fields['attachmentKey']
        );
    }
}
