<?php namespace GraideNetwork\Base\Integrators;

use GraideNetwork\Base\Clients\CoursesClient;

class CoursesIntegrator extends AbstractIntegrator
{
    protected $client;

    public function __construct(CoursesClient $client)
    {
        $this->client = $client;
    }

    /**
     * Attaches courses to an array of items via foreign key relationships
     *
     * @param array $items
     * @param array $fields
     *
     * @return array
     */
    public function attachCourses($items = [], $fields = [])
    {
        $this->defaultFields = [
            'itemAttachmentField' => 'course',
            'itemForeignKey' => 'course_id',
            'attachmentKey' => 'id',
        ];
        return $this->callAndAttach(function ($ids) {
            // Get all courses from the API
            try {
                return $this->client->getCourses([
                    'ids' => $ids,
                    'with' => ['grade', 'subject'],
                    'per_page' => count($ids),
                ])['data'];
            } catch (\Exception $e) {
                return [];
            }
        }, $items, $fields);
    }

    /**
     * Attaches sections to an array of items via foreign key relationships
     *
     * @param array $items
     * @param array $fields
     *
     * @return array
     */
    public function attachSections($items = [], $fields = [])
    {
        $this->defaultFields = [
            'itemAttachmentField' => 'section',
            'itemForeignKey' => 'section_id',
            'attachmentKey' => 'id',
        ];
        return $this->callAndAttach(function ($ids) {
            // Get all sections from the API
            try {
                return $this->client->getSections([
                    'ids' => $ids,
                    'with' => ['course'],
                    'per_page' => count($ids),
                ])['data'];
            } catch (\Exception $e) {
                return [];
            }
        }, $items, $fields);
    }

    /**
     * Attaches students to an array of items via foreign key relationships
     *
     * @param array $items
     * @param array $fields
     *
     * @return array
     */
    public function attachStudents($items = [], $fields = [])
    {
        $this->defaultFields = [
            'itemAttachmentField' => 'student',
            'itemForeignKey' => 'student_id',
            'attachmentKey' => 'id',
        ];
        return $this->callAndAttach(function ($ids) {
            // Get all students from the API
            try {
                return $this->client->getStudents([
                    'ids' => $ids,
                    'per_page' => count($ids),
                ])['data'];
            } catch (\Exception $e) {
                return [];
            }
        }, $items, $fields);
    }
}
