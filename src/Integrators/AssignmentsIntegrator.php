<?php namespace GraideNetwork\Base\Integrators;

use GraideNetwork\Base\Clients\AssignmentsClient;

class AssignmentsIntegrator extends AbstractIntegrator
{
    protected $client;

    public function __construct(AssignmentsClient $client)
    {
        $this->client = $client;
    }

    /**
     * Attaches assignments to an array of items via foreign key relationships
     *
     * @param array $items
     * @param array $fields
     *
     * @return array
     */
    public function attachAssignments($items = [], $fields = [])
    {
        $this->defaultFields = [
            'itemAttachmentField' => 'assignment',
            'itemForeignKey' => 'assignment_id',
            'attachmentKey' => 'id',
        ];
        return $this->callAndAttach(function ($ids) {
            // Get all users from the API
            try {
                return $this->client->getAssignments([
                    'ids' => $ids,
                    'with' => ['sectionAssignments', 'inquiries'],
                    'per_page' => count($ids),
                ])['data'];
            } catch (\Exception $e) {
                return [];
            }
        }, $items, $fields);
    }
}
