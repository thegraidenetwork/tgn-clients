<?php namespace GraideNetwork\Base\Integrators;

use GraideNetwork\Base\Clients\UsersClient;

class UsersIntegrator extends AbstractIntegrator
{
    protected $client;

    protected $defaultFields = [
        'itemAttachmentField' => 'user',
        'itemForeignKey' => 'user_id',
        'attachmentKey' => 'ID',
    ];

    public function __construct(UsersClient $client)
    {
        $this->client = $client;
    }

    /**
     * Attaches users to an array of items via foreign key relationships
     *
     * @param array $items
     * @param array $fields
     *
     * @return array
     */
    public function attachUsers($items = [], $fields = [])
    {
        return $this->callAndAttach(function ($ids) {
            // Get all users from the API
            try {
                return $this->client->getUsers([
                    'ids' => $ids,
                    'per_page' => count($ids),
                ])['data'];
            } catch (\Exception $e) {
                return [];
            }
        }, $items, $fields);
    }
}
