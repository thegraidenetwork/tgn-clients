<?php namespace GraideNetwork\Base\Clients;

class CoursesClient extends AbstractClient
{
    const RESOURCE_MAPPING = [
        // resource => endpoint    => type
        'course'   => ['courses'  => 'single'],
        'courses'  => ['courses'  => 'collection'],
        'grades'   => ['grades'   => 'collection'],
        'section'  => ['sections' => 'single'],
        'sections' => ['sections' => 'collection'],
        'student'  => ['students' => 'single'],
        'students' => ['students' => 'collection'],
        'subjects' => ['subjects' => 'collection'],
    ];

    public function __construct($userHeaders = [])
    {
        $this->initializeClient(
            getenv('ASSIGNMENTS_BASIC_AUTH_USERNAME'),
            getenv('ASSIGNMENTS_BASIC_AUTH_PASSWORD'),
            getenv('ASSIGNMENTS_URL'),
            $userHeaders
        );
    }
}
