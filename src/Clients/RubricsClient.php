<?php namespace GraideNetwork\Base\Clients;

class RubricsClient extends AbstractClient
{
    const RESOURCE_MAPPING = [
        // resource => endpoint    => type
        'rubric'    => ['rubrics'  => 'single'],
        'rubrics'   => ['rubrics'  => 'collection'],
    ];

    public function __construct($headers = [])
    {
        $this->initializeClient(
            getenv('ASSIGNMENTS_BASIC_AUTH_USERNAME'),
            getenv('ASSIGNMENTS_BASIC_AUTH_PASSWORD'),
            getenv('ASSIGNMENTS_URL'),
            $headers
        );
    }

    public function getRubricUsers($rubricId, $options = [])
    {
        $response = $this->client->get(
            "rubrics/{$rubricId}/users",
            ['query' => $options]
        );
        return $this->decodeResponse($response);
    }

    public function createRubricUser($rubricId, $data = [])
    {
        $response = $this->client->post(
            "rubrics/{$rubricId}/users",
            ['json' => $this->transformData($data)]
        );
        return $this->decodeResponse($response);
    }

    public function deleteRubricUser($rubricId, $userId)
    {
        $response = $this->client->delete("rubrics/{$rubricId}/users/{$userId}");
        if ($response->getStatusCode() == '204') {
            return true;
        }
        return false;
    }
}
