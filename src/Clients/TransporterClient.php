<?php namespace GraideNetwork\Base\Clients;

class TransporterClient extends AbstractClient
{
    const RESOURCE_MAPPING = [
        // resource                  => endpoint                      => type
        'export-graiders'            => ['export/graiders'            => 'single'],
        'export-section-assignments' => ['export/section-assignments' => 'single'],
        'export-student-results'     => ['export/student-results'     => 'single'],
        'export-teachers'            => ['export/teachers'            => 'single'],
        'export-transactions'        => ['export/transactions'        => 'single'],
        'import-students'            => ['import/students'            => 'single'],
        'student-reports'            => ['reports/create-student-reports' => 'single'],
        'student-reports-zip'        => ['reports/save-student-reports-zip' => 'single'],
    ];

    public function __construct($headers = [])
    {
        $this->initializeClient(
            getenv('ASSIGNMENTS_BASIC_AUTH_USERNAME'),
            getenv('ASSIGNMENTS_BASIC_AUTH_PASSWORD'),
            getenv('ASSIGNMENTS_URL'),
            $headers
        );
    }

    public function createImportStudents($file = null, $data = null)
    {
        $parts = [];
        $parts []= ['name' => 'students', 'contents' => $file];
        foreach ($data as $key => $value) {
            $parts []= ['name' => $key, 'contents' => $value];
        }
        $response = $this->client->request(
            'POST',
            'import/students',
            ['multipart' => $parts]
        );
        return $this->decodeResponse($response);
    }
}
