<?php namespace GraideNetwork\Base\Clients;

use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class ZapierClient
{
    const BASE_URL= 'https://hooks.zapier.com/hooks/catch/';

    /**
     * Guzzle client
     *
     * @var Client
     */
    public $client;

    /**
     * Optional, allows failures to be logged.
     *
     * @var LoggerInterface
     */
    public $logger;

    public function __construct(LoggerInterface $logger = null)
    {
        $this->client = new Client([
            'base_uri' => self::BASE_URL.getenv('ZAPIER_KEY').'/',
        ]);
        $this->logger = $logger;
    }

    /**
     * Post data to the Zapier hook
     *
     * @param string $hookId
     * @param array | object $data
     *
     * @return string | false
     */
    public function postHook($hookId, $data)
    {
        try {
            // Make the call
            $response = $this->client->post(
                $hookId, // The zapier hook ID
                ['json' => json_encode($data)]
            );
            // Return the response
            return json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $e) {
            if ($this->logger) {
                $this->logger->error($e->getMessage());
            }
        }
        return false;
    }
}
