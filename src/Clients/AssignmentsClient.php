<?php namespace GraideNetwork\Base\Clients;

class AssignmentsClient extends AbstractClient
{
    const RESOURCE_MAPPING = [
        // resource                  => endpoint                      => type
        'assignment'                 => ['assignments'                => 'single'],
        'assignments'                => ['assignments'                => 'collection'],
        'inquiry'                    => ['inquiries'                  => 'single'],
        'inquiries'                  => ['inquiries'                  => 'collection'],
        'section-assignment'         => ['section-assignments'        => 'single'],
        'section-assignments'        => ['section-assignments'        => 'collection'],
        'section-assignments-export' => ['section-assignments/export' => 'single'],
    ];

    public function __construct($userHeaders = [])
    {
        $this->initializeClient(
            getenv('ASSIGNMENTS_BASIC_AUTH_USERNAME'),
            getenv('ASSIGNMENTS_BASIC_AUTH_PASSWORD'),
            getenv('ASSIGNMENTS_URL'),
            $userHeaders
        );
    }

    public function getInquiriesByType($type = null, $options = [])
    {
        $response = $this->client->get(
            'inquiries/'.$type,
            ['query' => $options]
        );
        return $this->decodeResponse($response);
    }

    public function getSectionAssignmentsByStatus($status = null, $options = [])
    {
        $response = $this->client->get(
            'section-assignments/'.$status,
            ['query' => $options]
        );
        return $this->decodeResponse($response);
    }

    public function getStudentResultsForSectionAssignment($sectionAssignmentId = null, $options = [])
    {
        $response = $this->client->get(
            "section-assignments/{$sectionAssignmentId}/student-results",
            ['query' => $options]
        );
        return $this->decodeResponse($response);
    }
}
