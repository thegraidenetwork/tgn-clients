<?php namespace GraideNetwork\Base\Clients;

class UsersClient extends AbstractClient
{
    const RESOURCE_MAPPING = [
        // resource       => endpoint                  => type
        'conversations'   => ['messages/conversations' => 'collection'],
        'coupon'          => ['coupons'                => 'single'],
        'coupons'         => ['coupons'                => 'collection'],
        'favorite'        => ['favorites'              => 'single'],
        'graider-review'  => ['graider-reviews'        => 'single'],
        'graider-reviews' => ['graider-reviews'        => 'collection'],
        'message'         => ['messages'               => 'single'],
        'messages'        => ['messages'               => 'collection'],
        'order'           => ['orders'                 => 'collection'],
        'plans'           => ['plans'                  => 'collection'],
        'teacher-review'  => ['teacher-reviews'        => 'single'],
        'transaction'     => ['transactions'           => 'single'],
        'transactions'    => ['transactions'           => 'collection'],
        'user'            => ['users'                  => 'single'],
        'users'           => ['users'                  => 'collection'],
    ];

    public function __construct($userHeaders = [])
    {
        $this->initializeClient(
            getenv('ASSIGNMENTS_BASIC_AUTH_USERNAME'),
            getenv('ASSIGNMENTS_BASIC_AUTH_PASSWORD'),
            getenv('ASSIGNMENTS_URL'),
            $userHeaders
        );
    }

    public function getMessageHistory($user_id, $options = [])
    {
        $response = $this->client->get(
            'messages/history/'.$user_id,
            ['query' => $options]
        );
        return $this->decodeResponse($response);
    }

    public function postVerifyUser($token = null)
    {
        $response = $this->client->post(
            'users/verify/'.$token
        );
        return $this->decodeResponse($response);
    }

    public function postForgotPassword($email = null)
    {
        $response = $this->client->post(
            'auth/forgot-password',
            ['json' => $this->transformData(['email' => $email])]
        );
        return $this->decodeResponse($response);
    }

    public function postResetPassword($data = [])
    {
        $response = $this->client->post(
            'auth/reset-password',
            ['json' => $this->transformData($data)]
        );
        return $this->decodeResponse($response);
    }

    public function postLogin($data = [])
    {
        $response = $this->client->post(
            'auth/login',
            ['json' => $this->transformData($data)]
        );
        return $this->decodeResponse($response);
    }
}
