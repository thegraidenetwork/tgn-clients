<?php namespace GraideNetwork\Base\Clients;

use GuzzleHttp\Client;

abstract class AbstractClient
{
    const ACTION_MAPPING = [
        'create' => 'post',
        'delete' => 'delete',
        'get' => 'get',
        'update' => 'put',
        'patch' => 'patch',
    ];

    public $client;

    protected function initializeClient($username, $password, $baseUrl, $headers = [])
    {
        $this->client = new Client([
            'auth' => [$username, $password],
            'base_uri' => $baseUrl,
            'headers' => $headers,
        ]);
    }

    protected function decodeResponse($response)
    {
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * Transform array keys from camelCase to snake_case, for JSON's sake.
     */
    protected function transformData($data = [])
    {
        $transformedData = [];
        foreach ($data as $key => $value) {
            $snakeCaseKey = $this->snakify($key);
            $transformedData[$snakeCaseKey] = $value;
        }

        return $transformedData;
    }

    public function __call($name, $arguments)
    {
        $method = null;
        foreach (self::ACTION_MAPPING as $action => $httpVerb) {
            if (strpos($name, $action) === 0) {
                $resource = static::dasherize(substr($name, strlen($action)));
                $resourceMapping = static::RESOURCE_MAPPING;
                if (!isset($resourceMapping[$resource])) {
                    throw new \BadMethodCallException($resource . ' is not a recognized resource');
                }
                $resourceData = $resourceMapping[$resource];
                $method = $httpVerb;
                $endpoint = key($resourceData);
                $resourceType = $resourceData[$endpoint];
                break;
            }
        }
        if (!$method) {
            throw new \BadMethodCallException($name . ' is not a recognized method');
        }
        return $this->{$method}($endpoint, $resourceType, $arguments);
    }

    private function delete($endpoint, $resourceType, $arguments)
    {
        list($id) = $arguments;
        $response = $this->client->delete("{$endpoint}/{$id}");

        if ($response->getStatusCode() == '204') {
            return true;
        }
        return false;
    }

    private function get($endpoint, $resourceType, $arguments)
    {
        if ($resourceType === 'collection') {
            $data = count($arguments) ? array_shift($arguments) : [];
            $response = $this->client->get(
                $endpoint,
                ['query' => $data]
            );
        } else {
            $id = $arguments[0];
            $data = $arguments[1] ?? [];
            $response = $this->client->get(
                "{$endpoint}/{$id}",
                ['query' => $data]
            );
        }
        return $this->decodeResponse($response);
    }

    private function post($endpoint, $resourceType, $arguments)
    {
        list($data) = $arguments;
        $response = $this->client->post(
            $endpoint,
            ['json' => $this->transformData($data)]
        );
        return $this->decodeResponse($response);
    }

    private function patch($endpoint, $resourceType, $arguments)
    {
        list($id, $data) = $arguments;
        $response = $this->client->patch(
            "{$endpoint}/{$id}",
            ['json' => $this->transformData($data)]
        );

        if ($response->getStatusCode() == '204') {
            return true;
        }
        return false;
    }

    private function put($endpoint, $resourceType, $arguments)
    {
        list($id, $data) = $arguments;
        $response = $this->client->put(
            "{$endpoint}/{$id}",
            ['json' => $this->transformData($data)]
        );

        if ($response->getStatusCode() == '204') {
            return true;
        }
        return false;
    }

    public static function dasherize($string)
    {
        return self::replace($string, '-');
    }

    public static function snakify($string)
    {
        return self::replace($string, '_');
    }

    private static function replace($string, $replacement)
    {
        return ltrim(strtolower(preg_replace('/[A-Z]/', "{$replacement}$0", $string)), $replacement);
    }
}
