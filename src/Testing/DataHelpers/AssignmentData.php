<?php namespace GraideNetwork\Base\Testing\DataHelpers;

use Carbon\Carbon;

trait AssignmentData
{
    /**
     * Generates a mock API response for a call to getAssignment()
     *
     * @param array $assignmentData
     *
     * @return array
     */
    public function getAssignmentResponse(
        $assignmentData = [],
        $sectionAssignmentData = [],
        $sectionAssignmentCount = 0
    ) {
        $assignment = array_merge([
            'id' => rand(1, 100),
            'name' => uniqid(),
            'course_id' => rand(1, 100),
            'teacher_id' => rand(1, 100),
            'description' => uniqid(),
            'due_at' => Carbon::now()->addDays(3)->toDateTimeString(),
            'minutes_per_student' => rand(4, 15),
            'prompt_url' => uniqid(),
            'rubric_url' => uniqid(),
            'deleted_at' => null,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            'work_format' => 'Typed',
            'gradesheet_url' => uniqid(),
            'start_at' => Carbon::now()->toDateTimeString(),
            'feedback_needed' => 'On Report',
            'grades_needed' => 'Components And Total',
            'calculation_information' => '',
            'distribution_information' => '',
            'rationale_enabled' => 0,
            'rubric_information' => '',
            'rubric_id' => rand(1, 100),
            'grading_rubric' => null,
            'section_assignments_count' => rand(1, 5),
            'teacher_minutes_per_section' => rand(10, 100),
        ], $assignmentData);

        if ($sectionAssignmentCount > 0) {
            $assignment['section_assignments'] = $this->getSectionAssignmentsResponse(
                array_merge($sectionAssignmentData, [
                    'assignment_id' => $assignment['id']
                ]),
                $sectionAssignmentCount
            )['data'];
        }

        return $assignment;
    }

    /**
     * Generates a mock API response for a call to getAssignments()
     *
     * @param array $assignmentData
     * @param integer $assignmentCount
     * @param array $sectionAssignmentData
     * @param integer $sectionAssignmentCount
     *
     * @return array
     */
    public function getAssignmentsResponse(
        $assignmentData = [],
        $assignmentCount = 1,
        $sectionAssignmentData = [],
        $sectionAssignmentCount = 0
    ) {
        $assignments = [];
        for ($a = 0; $a < $assignmentCount; $a++) {
            $assignments[$a] = $this->getAssignmentResponse(
                $assignmentData,
                $sectionAssignmentData,
                $sectionAssignmentCount
            );
        }
        return ['data' => $assignments];
    }

    /**
     * Generates a mock API response for a call to getSectionAssignment()
     *
     * @param array $sectionAssignmentData
     * @param array $assignmentData
     *
     * @return array
     */
    public function getSectionAssignmentResponse(
        $sectionAssignmentData = [],
        $assignmentData = []
    ) {
        $sectionAssignment = array_merge([
            'id' => rand(1, 100),
            'assignment_id' => rand(1, 100),
            'section_id' => rand(1, 100),
            'student_work_url' => uniqid(),
            'submitted_to_teacher_at' => null,
            'deleted_at' => null,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            'graider_id' => rand(1, 100),
            'status' => 'In Progress',
            'completed_at' => null,
            'graider_reported_hours_to_grade' => null,
            'graded_student_work_url' => null,
            'minutes_to_grade' => '46.00',
            'areas_of_growth' => null,
            'areas_of_strength' => null,
            'grading_status' => 'Not Started',
            'teacher_revision_requested_at' => null,
            'graded_students_count' => 0,
            'user_status' => 'In Progress',
        ], $sectionAssignmentData);

        // Append the parent assignment
        if ($assignmentData) {
            $sectionAssignment['assignment'] = $this->getAssignmentResponse(
                array_merge($assignmentData, ['id' => $sectionAssignment['assignment_id']])
            );
        }

        return $sectionAssignment;
    }

    /**
     * Generates a mock API response for a call to getSectionAssignments()
     *
     * @param array $sectionAssignmentData
     * @param integer $sectionAssignmentCount
     * @param array $assignmentData
     *
     * @return array
     */
    public function getSectionAssignmentsResponse(
        $sectionAssignmentData = [],
        $sectionAssignmentCount = 0,
        $assignmentData = []
    ) {
        $sectionAssignments = [];
        for ($a = 0; $a < $sectionAssignmentCount; $a++) {
            $sectionAssignments[$a] = $this->getSectionAssignmentResponse(
                $sectionAssignmentData,
                $assignmentData
            );
        }
        return ['data' => $sectionAssignments];
    }
}
