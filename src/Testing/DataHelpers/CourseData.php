<?php namespace GraideNetwork\Base\Testing\DataHelpers;

use Carbon\Carbon;

trait CourseData
{
    /**
     * Generates a mock API response for a call to getCourse()
     *
     * @param array $data
     *
     * @return array
     */
    public function getCourseResponse($data = [])
    {
        return array_merge([
            'id' => rand(1, 100),
            'name' => uniqid(),
            'user_id' => rand(1, 100),
            'subject_id' => rand(1, 100),
            'grade_id' => rand(1, 100),
            'deleted_at' => null,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            'student_count' => rand(10, 50),
        ], $data);
    }

    /**
     * Generates a mock API response for a call to getCourses()
     *
     * @param array $data
     * @param integer $count
     *
     * @return array
     */
    public function getCoursesResponse(
        $data = [],
        $count = 1
    ) {
        $results = [];
        for ($a = 0; $a < $count; $a++) {
            $results[$a] = $this->getCourseResponse($data);
        }
        return ['data' => $results];
    }
}
