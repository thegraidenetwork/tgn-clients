<?php namespace GraideNetwork\Base\Testing\DataHelpers;

use Carbon\Carbon;

trait UserData
{
    /**
     * Generates a mock API response for a call to getTransaction()
     *
     * @param array $data
     *
     * @return array
     */
    public function getTransactionResponse($data = [], $userData = [])
    {
        $transaction = array_merge([
            'id' => rand(1, 100),
            'assignment_id' => rand(1, 100),
            'teacher_id' => rand(1, 100),
            'type' => 'Debit',
            'hours' => rand(2, 20).'.00',
            'number_of_sections' => rand(2, 5),
            'number_of_students' => rand(10, 50),
            'minutes_per_student' => rand(2, 5),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ], $data);

        // Append the teacher
        if ($userData) {
            $transaction['teacher'] = $this->getUserResponse(
                array_merge($userData, ['ID' => $transaction['teacher_id']])
            );
        }

        return $transaction;
    }

    /**
     * Generates a mock API response for a call to getTransactions()
     *
     * @param array $data
     * @param integer $userCount
     *
     * @return array
     */
    public function getTransactionsResponse(
        $data = [],
        $count = 1,
        $userData = []
    ) {
        $results = [];
        for ($a = 0; $a < $count; $a++) {
            $results[$a] = $this->getTransactionResponse($data, $userData);
        }
        return ['data' => $results];
    }

    /**
     * Generates a mock API response for a call to getUser()
     *
     * @param array $data
     *
     * @return array
     */
    public function getUserResponse($data = [])
    {
        return array_merge([
            'ID' => rand(1, 100),
            'email' => 'text@example.com',
            'first_name' => uniqid(),
            'last_name' => uniqid(),
            'dp' => '',
            'dob' => '',
            'hours_per_month' => '',
            'info' => '',
            'type' => 't',
            'created_on' => Carbon::now()->toDateTimeString(),
            'modified_on' => Carbon::now()->toDateTimeString(),
            'login_on' => Carbon::now()->toDateTimeString(),
            'reffered_by' => 0,
            'quota' => 0,
            'status' => 1,
            'verified' => 1,
            'email_verified' => 1,
            'varification_step' => 0,
            'phone_number' => '',
            'added_hours_at' => null,
            'admin_score' => 0,
            'average_accuracy' => null,
            'average_overall' => null,
            'average_professionalism' => null,
            'average_quality' => null,
        ], $data);
    }

    /**
     * Generates a mock API response for a call to getUsers()
     *
     * @param array $data
     * @param integer $userCount
     *
     * @return array
     */
    public function getUsersResponse(
        $data = [],
        $count = 1
    ) {
        $results = [];
        for ($a = 0; $a < $count; $a++) {
            $results[$a] = $this->getUserResponse($data);
        }
        return ['data' => $results];
    }
}
