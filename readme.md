# API Clients

### The Graide Network

This package contains internal API clients for The Graide Network's private API.

## Installation

To get the latest stable version of this package add this line to your project's `composer.json` file:

```
 "thegraidenetwork/tgn-clients": "^3.3"
```

You may also specify any version release listed on [Packagist](https://packagist.org/packages/thegraidenetwork/tgn-clients). Check the [Changelog](changelog.md) for an overview of changes.

## Files

### Clients
Use each client to connect with a different Graide Network API.

- **AbstractClient** - Serves as a standard base class for most internal API clients.
- **AssignmentsClient** - Allows access to the assignments API.
- **CoursesClient** - Allows access to the courses API.
- **RubricsClient** - Allows access to the rubrics API.
- **TransporterClient** - Allows access to the transporter API.
- **UsersClient** - Allows access to the users API.

### Integrators
Integrators make merging data from multiple APIs simpler.

- **AbstractIntegrator** - Serves as a standard base class for all integrators.
- **AssignmentsIntegrator** - Merges assignments into an associated data model.
- **CoursesIntegrator** - Merges courses, sections, or students into an associated data model.
- **UsersIntegrator** - Merges users into an associated data model.

## Updating this package
- Make changes.
- Add your updates to the [the changelog file](changelog.md).
- Update the readme.md file with any new information.
- Commit your changes.
- Create and push a tag: `git tag <SEM_VER>` then `git push origin <SEM_VER>`.
- Push your changes to the master branch: `git push origin master`.

## Tests

The test suite for this project can be run with the command: `npm run app:test`.

And the linter can be run with the command: `npm run app:lint`.

Tests for the abstract classes in this package use concrete fixtures to instantiate new objects. A code coverage report is available in the `build/` directory after the first time tests are run.

## Releases
- See [the changelog](changelog.md).
