<?php namespace GraideNetwork\Base\Tests\Unit\Clients;

use GraideNetwork\Base\Clients\UsersClient;
use GraideNetwork\Base\Tests\UnitTestCase;
use Mockery as m;

class UsersClientTest extends UnitTestCase
{
    public function setUp(): void
    {
        $this->fixturesDir = $this->getFixturesDir();
        $headers = [
            'X-User-Id' => rand(1, 5),
            'X-User-Role' => uniqid(),
        ];
        $this->client = new UsersClient($headers);
        $this->client->client = m::mock('GuzzleHttp\Client');
        $this->response = m::mock('GuzzleHttp\Psr7\Response');
    }

    public function testItCanGetUserById()
    {
        $id = rand(1, 50);
        $options = [];

        $user = (object) [
            'id' => $id,
            'email' => uniqid(),
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("users/{$id}", ['query' => []])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($user));

        $results = $this->client->getUser($id, $options);

        $this->assertEquals($results['id'], $user->id);
        $this->assertEquals($results['email'], $user->email);
    }

    public function testItCanGetUserByIdWithNoOptions()
    {
        $id = rand(1, 50);

        $user = (object) [
            'id' => $id,
            'email' => uniqid(),
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("users/{$id}", ['query' => []])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($user));

        $results = $this->client->getUser($id);

        $this->assertEquals($results['id'], $user->id);
        $this->assertEquals($results['email'], $user->email);
    }

    public function testItThrowsExceptionWhenItCannotGetUserById()
    {
        $this->expectException(\Exception::class);

        $id = rand(1, 50);
        $options = [];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("users/{$id}", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andThrow('\Exception');

        $this->client->getUser($id, $options);
    }

    public function testItCanGetPaginatedUsers()
    {
        $options = [];
        $users = file_get_contents($this->fixturesDir."clients/get-users.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("users", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($users);

        $results = $this->client->getUsers($options);

        $this->assertEquals(json_decode($users)->total, count($results['data']));
    }

    public function testItCanGetPaginatedUsersWithOptions()
    {
        $options = [
            'page' => 2,
        ];
        $users = file_get_contents($this->fixturesDir."clients/get-users.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("users", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($users);

        $results = $this->client->getUsers($options);

        $this->assertEquals(json_decode($users)->total, count($results['data']));
    }

    public function testItThrowsErrorWhenGettingUsersFails()
    {
        $this->expectException(\Exception::class);

        $options = [
            'page' => 2,
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("users", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andThrow('\Exception');

        $this->client->getUsers($options);
    }

    public function testItCanGetMessageById()
    {
        $id = rand(1, 50);
        $options = [];

        $message = (object) [
            'id' => $id,
            'message' => uniqid(),
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("messages/{$id}", ['query' => []])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($message));

        $results = $this->client->getMessage($id, $options);

        $this->assertEquals($results['id'], $message->id);
        $this->assertEquals($results['message'], $message->message);
    }

    public function testItThrowsExceptionWhenItCannotGetMessageById()
    {
        $this->expectException(\Exception::class);

        $id = rand(1, 50);
        $options = [];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("messages/{$id}", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andThrow('\Exception');

        $this->client->getMessage($id, $options);
    }

    public function testItCanGetPaginatedMessages()
    {
        $options = [];
        $messages = file_get_contents($this->fixturesDir."clients/get-messages.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("messages", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($messages);

        $results = $this->client->getMessages($options);

        $this->assertEquals(json_decode($messages)->per_page, count($results['data']));
    }

    public function testItCanGetPaginatedMessagesWithOptions()
    {
        $options = [
            'page' => 2,
        ];
        $messages = file_get_contents($this->fixturesDir."clients/get-messages.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("messages", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($messages);

        $results = $this->client->getMessages($options);

        $this->assertEquals(json_decode($messages)->per_page, count($results['data']));
    }

    public function testItThrowsErrorWhenGettingMessagesFails()
    {
        $this->expectException(\Exception::class);

        $options = [
            'page' => 2,
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("messages", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andThrow('\Exception');

        $this->client->getMessages($options);
    }

    public function testItCanGetPaginatedMessageHistoryWithOptions()
    {
        $userId = rand(1, 50);
        $options = [
            'page' => 2,
        ];
        $messages = file_get_contents($this->fixturesDir."clients/get-messages.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("messages/history/{$userId}", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($messages);

        $results = $this->client->getMessageHistory($userId, $options);

        $this->assertEquals(json_decode($messages)->per_page, count($results['data']));
    }

    public function testItCanGetPaginatedConversationsWithOptions()
    {
        $userId = rand(1, 50);
        $options = [
            'page' => 2,
        ];
        $messages = file_get_contents("{$this->fixturesDir}clients/get-messages.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with('messages/conversations', ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($messages);

        $results = $this->client->getConversations($options);

        $this->assertEquals(json_decode($messages)->per_page, count($results['data']));
    }

    public function testItCanVerifyUserWithToken()
    {
        $token = uniqid();
        $user = ['ID' => uniqid()];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("users/verify/{$token}")
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($user));

        $results = $this->client->postVerifyUser($token);

        $this->assertEquals($results, $user);
    }

    public function testItCanForgotPasswordWithEmail()
    {
        $email = uniqid();

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("auth/forgot-password", ['json' => ['email' => $email]])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(null);

        $results = $this->client->postForgotPassword($email);
        $this->assertNull($results);
    }

    public function testItCanResetPassword()
    {
        $data = [
            'token' => uniqid(),
            'password' => uniqid(),
            'confirm_password' => uniqid(),
        ];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("auth/reset-password", ['json' => $data])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(null);

        $results = $this->client->postResetPassword($data);
        $this->assertNull($results);
    }

    public function testItCanLoginUser()
    {
        $data = [
            'password' => uniqid(),
            'email' => uniqid(),
        ];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("auth/login", ['json' => $data])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode([
                'ID' => uniqid(),
                'email' => $data['email'],
                'first_name' => uniqid(),
                'last_name' => uniqid(),
            ]));

        $results = $this->client->postLogin($data);

        $this->assertEquals($results['email'], $data['email']);
    }
}
