<?php namespace GraideNetwork\Base\Tests\Unit\Clients;

use GraideNetwork\Base\Clients\RubricsClient;
use GraideNetwork\Base\Tests\UnitTestCase;
use Mockery as m;

class RubricsClientTest extends UnitTestCase
{
    public function setUp(): void
    {
        $this->fixturesDir = $this->getFixturesDir();
        $this->client = new RubricsClient([]);
        $this->client->client = m::mock('GuzzleHttp\Client');
        $this->response = m::mock('GuzzleHttp\Psr7\Response');
    }

    public function testItCanGetRubricById()
    {
        $id = rand(1, 50);
        $options = [];

        $rubric = (object) [
            'id' => $id,
            'name' => uniqid(),
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("rubrics/{$id}", ['query' => []])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($rubric));

        $results = $this->client->getRubric($id, $options);

        $this->assertEquals($results['id'], $rubric->id);
        $this->assertEquals($results['name'], $rubric->name);
    }

    public function testItThrowsExceptionWhenItCannotGetRubricById()
    {
        $this->expectException(\Exception::class);

        $id = rand(1, 50);
        $options = [];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("rubrics/{$id}", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andThrow('\Exception');

        $this->client->getRubric($id, $options);
    }

    public function testItCanGetPaginatedRubrics()
    {
        $options = [];
        $courses = file_get_contents($this->fixturesDir."clients/get-rubrics.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("rubrics", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($courses);

        $results = $this->client->getRubrics($options);

        $this->assertEquals(1, count($results['data']));
    }

    public function testItCanCreateRubricWithValidData()
    {
        $data = [
            'name' => uniqid(),
            'description' => uniqid(),
        ];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("rubrics", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($data));

        $results = $this->client->createRubric($data);

        $this->assertEquals($results['name'], $data['name']);
        $this->assertEquals($results['description'], $data['description']);
    }

    public function testItCanGetRubricUsers()
    {
        $rubricId = rand(1, 20);
        $options = [];
        $rubricUsers = file_get_contents($this->fixturesDir."clients/get-rubric-users.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("rubrics/{$rubricId}/users", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($rubricUsers);

        $results = $this->client->getRubricUsers($rubricId, $options);

        $this->assertEquals(3, count($results));
    }

    public function testItCanCreateRubricUser()
    {
        $rubricId = rand(1, 20);
        $data = ['user_id' => rand(1, 20)];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("rubrics/{$rubricId}/users", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($data));

        $results = $this->client->createRubricUser($rubricId, $data);

        $this->assertEquals($results['user_id'], $data['user_id']);
    }

    public function testItCanDeleteRubricUser()
    {
        $rubricId = rand(1, 20);
        $userId = rand(1, 20);

        $this->client->client->shouldReceive('delete')
            ->once()
            ->with("rubrics/{$rubricId}/users/{$userId}")
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn(204);

        $results = $this->client->deleteRubricUser($rubricId, $userId);

        $this->assertTrue($results);
    }
}
