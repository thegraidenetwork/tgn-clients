<?php namespace GraideNetwork\Base\Tests\Unit\Clients;

use GraideNetwork\Base\Clients\AssignmentsClient;
use GraideNetwork\Base\Tests\UnitTestCase;
use Mockery as m;

class AssignmentsClientTest extends UnitTestCase
{
    public function setUp(): void
    {
        $this->fixturesDir = $this->getFixturesDir();
        $headers = [
            'X-User-Id' => rand(1, 5),
            'X-User-Role' => uniqid(),
        ];
        $this->client = new AssignmentsClient($headers);
        $this->client->client = m::mock('GuzzleHttp\Client');
        $this->response = m::mock('GuzzleHttp\Psr7\Response');
    }

    public function testItCanGetAssignmentById()
    {
        $id = rand(1, 50);
        $options = [];

        $assignment = (object) [
            'id' => $id,
            'name' => uniqid(),
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("assignments/{$id}", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($assignment));

        $results = $this->client->getAssignment($id, $options);

        $this->assertEquals($results['id'], $assignment->id);
        $this->assertEquals($results['name'], $assignment->name);
    }

    public function testItThrowsExceptionWhenItCannotGetAssignmentById()
    {
        $this->expectException(\Exception::class);
        $id = rand(1, 50);
        $options = [];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("assignments/{$id}", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andThrow('\Exception');

        $this->client->getAssignment($id, $options);
    }

    public function testItCanGetPaginatedAssignments()
    {
        $options = [];
        $assignments = file_get_contents($this->fixturesDir."clients/get-assignments.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("assignments", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($assignments);

        $results = $this->client->getAssignments($options);

        $this->assertEquals(1, count($results['data']));
    }

    public function testItCanGetPaginatedAssignmentsWithOptions()
    {
        $options = [
            'filters' => [
                'user_id' => 1,
            ],
            'page' => 2,
        ];
        $assignments = file_get_contents($this->fixturesDir."clients/get-assignments.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("assignments", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($assignments);

        $results = $this->client->getAssignments($options);

        $this->assertEquals(1, count($results['data']));
    }

    public function testItCanGetSectionAssignmentsByStatus()
    {
        $status = uniqid();
        $options = [];
        $assignments = file_get_contents($this->fixturesDir."clients/get-assignments.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("section-assignments/{$status}", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($assignments);

        $results = $this->client->getSectionAssignmentsByStatus($status, $options);

        $this->assertEquals(1, count($results['data']));
    }

    public function testItCanGetInquiriesByType()
    {
        $type = uniqid();
        $options = [];
        $assignments = file_get_contents($this->fixturesDir."clients/get-assignments.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("inquiries/{$type}", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($assignments);

        $results = $this->client->getInquiriesByType($type, $options);

        $this->assertEquals(1, count($results['data']));
    }

    public function testItThrowsErrorWhenGettingAssignmentsFails()
    {
        $this->expectException(\Exception::class);

        $options = [
            'filters' => [
                'user_id' => 1,
            ],
            'page' => 2,
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("assignments", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andThrow('\Exception');

        $this->client->getAssignments($options);
    }

    public function testItCanCreateAssignmentWithValidData()
    {
        $data = [
            'name' => uniqid(),
            'user_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("assignments", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($data));

        $results = $this->client->createAssignment($data);

        $this->assertEquals($results['name'], $data['name']);
        $this->assertEquals($results['user_id'], $data['user_id']);
    }

    public function testItReturnsTrueWhenCanUpdateAssignment()
    {
        $id = rand(1, 50);
        $data = [
            'name' => uniqid(),
            'user_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('put')
            ->once()
            ->with("assignments/{$id}", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('204');

        $results = $this->client->updateAssignment($id, $data);

        $this->assertTrue($results);
    }

    public function testItReturnsFalseWhenCannotUpdateAssignment()
    {
        $id = rand(1, 50);
        $data = [
            'name' => uniqid(),
            'user_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('put')
            ->once()
            ->with("assignments/{$id}", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('400');

        $results = $this->client->updateAssignment($id, $data);

        $this->assertFalse($results);
    }

    public function testItReturnsTrueWhenCanDeleteAssignment()
    {
        $id = rand(1, 50);

        $this->client->client->shouldReceive('delete')
            ->once()
            ->with("assignments/{$id}")
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('204');

        $results = $this->client->deleteAssignment($id);

        $this->assertTrue($results);
    }

    public function testItReturnsFalseWhenCannotDeleteAssignment()
    {
        $id = rand(1, 50);

        $this->client->client->shouldReceive('delete')
            ->once()
            ->with("assignments/{$id}")
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('400');

        $results = $this->client->deleteAssignment($id);

        $this->assertFalse($results);
    }

    public function testItCanCreateSectionAssignmentWithValidData()
    {
        $data = [
            'assignment_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("section-assignments", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($data));

        $results = $this->client->createSectionAssignment($data);

        $this->assertEquals($results['assignment_id'], $data['assignment_id']);
    }

    public function testItReturnsTrueWhenCanUpdateSectionAssignment()
    {
        $id = rand(1, 50);
        $data = [
            'assignment_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('put')
            ->once()
            ->with("section-assignments/{$id}", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('204');

        $results = $this->client->updateSectionAssignment($id, $data);

        $this->assertTrue($results);
    }

    public function testItReturnsFalseWhenCannotUpdateSectionAssignment()
    {
        $id = rand(1, 50);
        $data = [
            'assignment_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('put')
            ->once()
            ->with("section-assignments/{$id}", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('400');

        $results = $this->client->updateSectionAssignment($id, $data);

        $this->assertFalse($results);
    }

    public function testItReturnsTrueWhenCanDeleteSectionAssignment()
    {
        $id = rand(1, 50);

        $this->client->client->shouldReceive('delete')
            ->once()
            ->with("section-assignments/{$id}")
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('204');

        $results = $this->client->deleteSectionAssignment($id);

        $this->assertTrue($results);
    }

    public function testItReturnsFalseWhenCannotDeleteSectionAssignment()
    {
        $id = rand(1, 50);

        $this->client->client->shouldReceive('delete')
            ->once()
            ->with("section-assignments/{$id}")
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('400');

        $results = $this->client->deleteSectionAssignment($id);

        $this->assertFalse($results);
    }

    public function testItCanCreateAnInquiryWithValidData()
    {
        $data = [
            'assignment_id' => uniqid(),
            'graider_id' => uniqid(),
            'type' => 'FAKE TYPE',
        ];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with('inquiries', ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($data));

        $result = $this->client->createInquiry($data);
        $this->assertEquals($result['assignment_id'], $data['assignment_id']);
        $this->assertEquals($result['graider_id'], $data['graider_id']);
        $this->assertEquals($result['type'], $data['type']);
    }

    public function testItCanGetStudentResultsForSectionAssignment()
    {
        $sectionAssignmentId = rand(1, 50);
        $options = [
            uniqid() => uniqid(),
        ];
        $studentResults = [
            uniqid() => uniqid(),
            uniqid() => uniqid(),
            uniqid() => uniqid(),
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("section-assignments/{$sectionAssignmentId}/student-results", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode(['data' => $studentResults]));

        $results = $this->client->getStudentResultsForSectionAssignment($sectionAssignmentId, $options);

        $this->assertEquals($studentResults, $results['data']);
    }
}
