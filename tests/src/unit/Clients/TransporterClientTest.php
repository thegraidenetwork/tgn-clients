<?php namespace GraideNetwork\Base\Tests\Unit\Clients;

use GraideNetwork\Base\Clients\TransporterClient;
use GraideNetwork\Base\Tests\UnitTestCase;
use Mockery as m;

class TransporterClientTest extends UnitTestCase
{
    public function setUp(): void
    {
        $this->client = new TransporterClient([]);
        $this->client->client = m::mock('GuzzleHttp\Client');
        $this->response = m::mock('GuzzleHttp\Psr7\Response');
    }

    /**
     * @dataProvider exportProvider
     */
    public function testItCanExport($endpoint, $method)
    {
        $data = [
            'requester_id' => rand(1, 50),
        ];
        $message = 'Export queued for processing.';

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("export/{$endpoint}", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode(['message' => $message]));

        $results = $this->client->$method($data);

        $this->assertEquals($message, $results['message']);
    }

    public function testItCanGenerateStudentReports()
    {
        $data = [
            'section_assignment_id' => rand(1, 50),
        ];
        $message = 'Report queued for processing.';

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("reports/create-student-reports", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode(['message' => $message]));

        $results = $this->client->createStudentReports($data);

        $this->assertEquals($message, $results['message']);
    }

    public function testItCanImportStudents()
    {
        $fileContents = uniqid();
        $metadata = ['key' => 'value'];
        $expectedParts = [
            ['name' => 'students', 'contents' => $fileContents],
            ['name' => 'key', 'contents' => 'value'],
        ];
        $expectedMessage = 'Import queued for processing.';

        $this->client->client->shouldReceive('request')
            ->once()
            ->with(
                'POST',
                'import/students',
                ['multipart' => $expectedParts]
            )
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode(['status' => $expectedMessage]));

        $results = $this->client->createImportStudents($fileContents, $metadata);
        $this->assertEquals($expectedMessage, $results['status']);
    }

    public function exportProvider()
    {
        return [
            // endpoint, method
            ['graiders', 'createExportGraiders'],
            ['section-assignments', 'createExportSectionAssignments'],
            ['student-results', 'createExportStudentResults'],
            ['teachers', 'createExportTeachers'],
            ['transactions', 'createExportTransactions'],
        ];
    }
}
