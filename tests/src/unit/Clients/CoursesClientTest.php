<?php namespace GraideNetwork\Base\Tests\Unit\Clients;

use GraideNetwork\Base\Clients\CoursesClient;
use GraideNetwork\Base\Tests\UnitTestCase;
use Mockery as m;

class CoursesClientTest extends UnitTestCase
{
    public function setUp(): void
    {
        $this->fixturesDir = $this->getFixturesDir();
        $headers = [
            'X-User-Id' => rand(1, 5),
            'X-User-Role' => uniqid(),
        ];
        $this->client = new CoursesClient($headers);
        $this->client->client = m::mock('GuzzleHttp\Client');
        $this->response = m::mock('GuzzleHttp\Psr7\Response');
    }

    public function testItCanGetCourseById()
    {
        $id = rand(1, 50);
        $options = [];

        $course = (object) [
            'id' => $id,
            'name' => uniqid(),
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("courses/{$id}", ['query' => []])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($course));

        $results = $this->client->getCourse($id, $options);

        $this->assertEquals($results['id'], $course->id);
        $this->assertEquals($results['name'], $course->name);
    }

    public function testItThrowsExceptionWhenItCannotGetCourseById()
    {
        $this->expectException(\Exception::class);

        $id = rand(1, 50);
        $options = [];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("courses/{$id}", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andThrow('\Exception');

        $this->client->getCourse($id, $options);
    }

    public function testItCanGetPaginatedCourses()
    {
        $options = [];
        $courses = file_get_contents($this->fixturesDir."clients/get-courses.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("courses", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($courses);

        $results = $this->client->getCourses($options);

        $this->assertEquals(1, count($results['data']));
    }

    public function testItCanGetPaginatedCoursesWithOptions()
    {
        $options = [
            'filters' => [
                'user_id' => 1,
            ],
            'page' => 2,
        ];
        $courses = file_get_contents($this->fixturesDir."clients/get-courses.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("courses", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($courses);

        $results = $this->client->getCourses($options);

        $this->assertEquals(1, count($results['data']));
    }

    public function testItThrowsErrorWhenGettingCoursesFails()
    {
        $this->expectException(\Exception::class);

        $options = [
            'filters' => [
                'user_id' => 1,
            ],
            'page' => 2,
        ];

        $this->client->client->shouldReceive('get')
            ->once()
            ->with("courses", ['query' => $options])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andThrow('\Exception');

        $this->client->getCourses($options);
    }

    public function testItCanCreateCourseWithValidData()
    {
        $data = [
            'name' => uniqid(),
            'user_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("courses", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($data));

        $results = $this->client->createCourse($data);

        $this->assertEquals($results['name'], $data['name']);
        $this->assertEquals($results['user_id'], $data['user_id']);
    }

    public function testItReturnsTrueWhenCanUpdateCourse()
    {
        $id = rand(1, 50);
        $data = [
            'name' => uniqid(),
            'user_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('put')
            ->once()
            ->with("courses/{$id}", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('204');

        $results = $this->client->updateCourse($id, $data);

        $this->assertTrue($results);
    }

    public function testItReturnsFalseWhenCannotUpdateCourse()
    {
        $id = rand(1, 50);
        $data = [
            'name' => uniqid(),
            'user_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('put')
            ->once()
            ->with("courses/{$id}", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('400');

        $results = $this->client->updateCourse($id, $data);

        $this->assertFalse($results);
    }

    public function testItCanCreateSectionWithValidData()
    {
        $data = [
            'name' => uniqid(),
            'course_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with("sections", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($data));

        $results = $this->client->createSection($data);

        $this->assertEquals($results['name'], $data['name']);
        $this->assertEquals($results['course_id'], $data['course_id']);
    }

    public function testItReturnsTrueWhenCanUpdateSection()
    {
        $id = rand(1, 50);
        $data = [
            'name' => uniqid(),
            'course_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('put')
            ->once()
            ->with("sections/{$id}", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('204');

        $results = $this->client->updateSection($id, $data);

        $this->assertTrue($results);
    }

    public function testItReturnsFalseWhenCannotUpdateSection()
    {
        $id = rand(1, 50);
        $data = [
            'name' => uniqid(),
            'course_id' => rand(1, 50),
        ];

        $this->client->client->shouldReceive('put')
            ->once()
            ->with("sections/{$id}", ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getStatusCode')
            ->once()
            ->andReturn('400');

        $results = $this->client->updateSection($id, $data);

        $this->assertFalse($results);
    }

    public function testItCanGetGrades()
    {
        $grades = file_get_contents($this->fixturesDir."clients/get-grades.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with('grades', ['query' => []])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($grades);

        $results = $this->client->getGrades();

        $this->assertEquals(2, count($results));
    }

    public function testItCanGetSubjects()
    {
        $subjects = file_get_contents($this->fixturesDir."clients/get-subjects.json");

        $this->client->client->shouldReceive('get')
            ->once()
            ->with('subjects', ['query' => []])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn($subjects);

        $results = $this->client->getSubjects();

        $this->assertEquals(2, count($results));
    }

    public function testItCanCreateStudentWithValidData()
    {
        $data = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
            'section_id' => rand(1, 20),
        ];

        $this->client->client->shouldReceive('post')
            ->once()
            ->with('students', ['json' => $this->transformData($data)])
            ->andReturn($this->response);
        $this->response->shouldReceive('getBody')
            ->once()
            ->andReturn($this->response);
        $this->response->shouldReceive('getContents')
            ->once()
            ->andReturn(json_encode($data));

        $results = $this->client->createStudent($data);

        $this->assertEquals($results['first_name'], $data['first_name']);
        $this->assertEquals($results['last_name'], $data['last_name']);
        $this->assertEquals($results['section_id'], $data['section_id']);
    }
}
