<?php namespace GraideNetwork\Base\Tests\Unit\Integrators;

use GraideNetwork\Base\Integrators\UsersIntegrator;
use GraideNetwork\Base\Tests\UnitTestCase;
use Mockery as m;

class UsersIntegratorTest extends UnitTestCase
{
    public function setUp(): void
    {
        $this->client = m::mock('GraideNetwork\Base\Clients\UsersClient');
        $this->integrator = new UsersIntegrator($this->client);
    }

    public function testItCanAttachCoursesToArray()
    {
        $items = [
            ['id' => uniqid(), 'user_id' => uniqid()],
            ['id' => uniqid(), 'user_id' => uniqid()],
            ['id' => uniqid(), 'user_id' => uniqid()],
        ];
        $userIds = array_map(function ($item) {
            return $item['user_id'];
        }, $items);
        $users = array_map(function ($userId) {
            return [
                'ID' => $userId,
                'first_name' => uniqid(),
                'last_name' => uniqid(),
            ];
        }, $userIds);

        $this->client->shouldReceive('getUsers')
            ->with([
                'ids' => $userIds,
                'per_page' => count($userIds),
            ])
            ->once()
            ->andReturn(['data' => $users]);

        $results = $this->integrator->attachUsers($items);

        $this->assertEquals(count($items), count($results));
        foreach ($results as $result) {
            $this->assertEquals($result['user_id'], $result['user']['ID']);
        }
    }

    public function testItDoesNotAttachCoursesWhenExceptionThrown()
    {
        $items = [
            ['id' => uniqid(), 'user_id' => uniqid()],
            ['id' => uniqid(), 'user_id' => uniqid()],
            ['id' => uniqid(), 'user_id' => uniqid()],
        ];
        $userIds = array_map(function ($item) {
            return $item['user_id'];
        }, $items);

        $this->client->shouldReceive('getUsers')
            ->with([
                'ids' => $userIds,
                'per_page' => count($userIds),
            ])
            ->once()
            ->andThrow(\Exception::class);

        $results = $this->integrator->attachUsers($items);

        $this->assertEquals($items, $results);
    }
}
