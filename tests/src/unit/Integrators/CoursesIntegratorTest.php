<?php namespace GraideNetwork\Base\Tests\Unit\Integrators;

use GraideNetwork\Base\Integrators\CoursesIntegrator;
use GraideNetwork\Base\Tests\UnitTestCase;
use Mockery as m;

class CoursesIntegratorTest extends UnitTestCase
{
    public function setUp(): void
    {
        $this->client = m::mock('GraideNetwork\Base\Clients\CoursesClient');
        $this->integrator = new CoursesIntegrator($this->client);
    }

    public function testItCanAttachCoursesToArray()
    {
        $items = [
            ['id' => uniqid(), 'course_id' => uniqid()],
            ['id' => uniqid(), 'course_id' => uniqid()],
            ['id' => uniqid(), 'course_id' => uniqid()],
        ];
        $courseIds = array_map(function ($item) {
            return $item['course_id'];
        }, $items);
        $courses = array_map(function ($courseId) {
            return [
                'id' => $courseId,
                'name' => uniqid(),
            ];
        }, $courseIds);

        $this->client->shouldReceive('getCourses')
            ->with([
                'ids' => $courseIds,
                'with' => ['grade', 'subject'],
                'per_page' => count($items),
            ])
            ->once()
            ->andReturn(['data' => $courses]);

        $results = $this->integrator->attachCourses($items);

        $this->assertEquals(count($items), count($results));
        foreach ($results as $result) {
            $this->assertEquals($result['course_id'], $result['course']['id']);
        }
    }

    public function testItDoesNotAttachCoursesWhenExceptionThrown()
    {
        $items = [
            ['id' => uniqid(), 'course_id' => uniqid()],
            ['id' => uniqid(), 'course_id' => uniqid()],
            ['id' => uniqid(), 'course_id' => uniqid()],
        ];
        $courseIds = array_map(function ($item) {
            return $item['course_id'];
        }, $items);

        $this->client->shouldReceive('getCourses')
            ->with([
                'ids' => $courseIds,
                'with' => ['grade', 'subject'],
                'per_page' => count($items),
            ])
            ->once()
            ->andThrow(\Exception::class);

        $results = $this->integrator->attachCourses($items);

        $this->assertEquals($items, $results);
    }

    public function testItCanAttachSectionsToArray()
    {
        $items = [
            ['id' => uniqid(), 'section_id' => uniqid()],
            ['id' => uniqid(), 'section_id' => uniqid()],
            ['id' => uniqid(), 'section_id' => uniqid()],
        ];
        $sectionIds = array_map(function ($item) {
            return $item['section_id'];
        }, $items);
        $sections = array_map(function ($courseId) {
            return [
                'id' => $courseId,
                'name' => uniqid(),
            ];
        }, $sectionIds);

        $this->client->shouldReceive('getSections')
            ->with([
                'ids' => $sectionIds,
                'with' => ['course'],
                'per_page' => count($sectionIds),
            ])
            ->once()
            ->andReturn(['data' => $sections]);

        $results = $this->integrator->attachSections($items);

        $this->assertEquals(count($items), count($results));
        foreach ($results as $result) {
            $this->assertEquals($result['section_id'], $result['section']['id']);
        }
    }

    public function testItDoesNotAttachSectionsWhenExceptionThrown()
    {
        $items = [
            ['id' => uniqid(), 'section_id' => uniqid()],
            ['id' => uniqid(), 'section_id' => uniqid()],
            ['id' => uniqid(), 'section_id' => uniqid()],
        ];
        $sectionIds = array_map(function ($item) {
            return $item['section_id'];
        }, $items);

        $this->client->shouldReceive('getSections')
            ->with([
                'ids' => $sectionIds,
                'with' => ['course'],
                'per_page' => count($sectionIds),
            ])
            ->once()
            ->andThrow(\Exception::class);

        $results = $this->integrator->attachSections($items);

        $this->assertEquals($items, $results);
    }

    public function testItCanAttachStudentsToArray()
    {
        $items = [
            ['id' => uniqid(), 'student_id' => uniqid()],
            ['id' => uniqid(), 'student_id' => uniqid()],
            ['id' => uniqid(), 'student_id' => uniqid()],
        ];
        $studentIds = array_map(function ($item) {
            return $item['student_id'];
        }, $items);
        $students = array_map(function ($studentId) {
            return [
                'id' => $studentId,
                'first_name' => uniqid(),
                'last_name' => uniqid(),
            ];
        }, $studentIds);

        $this->client->shouldReceive('getStudents')
            ->with([
                'ids' => $studentIds,
                'per_page' => count($studentIds),
            ])
            ->once()
            ->andReturn(['data' => $students]);

        $results = $this->integrator->attachStudents($items);

        $this->assertEquals(count($items), count($results));
        foreach ($results as $result) {
            $this->assertEquals($result['student_id'], $result['student']['id']);
        }
    }
}
