<?php namespace GraideNetwork\Base\Tests\Unit\Integrators;

use GraideNetwork\Base\Integrators\AbstractIntegrator;
use GraideNetwork\Base\Tests\UnitTestCase;

class AbstractIntegratorTest extends UnitTestCase
{
    public function testItCanAttachItemsToThemselves()
    {
        $items = [
            ['attachment_id' => 1],
            ['attachment_id' => 2],
            // no 'attachement_id' => 3
        ];
        $attachments = [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3],
        ];
        $expectedResult = [
            ['id' => 1],
            ['id' => 2],
            // no 'id' => 3
        ];

        $integratedItems = AbstractIntegrator::attachItems(
            $items,
            $attachments,
            AbstractIntegrator::ATTACH_TO_SELF,  // `$itemAttachmentField`
            'attachment_id'  // `$attachmentKey`
        );

        $this->assertEquals($expectedResult, $integratedItems);
    }
}
