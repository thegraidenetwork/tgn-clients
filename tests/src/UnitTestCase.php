<?php namespace GraideNetwork\Base\Tests;

class UnitTestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * Allows you to get the value of a protected property via reflection.
     *
     * @param $object
     * @param $property
     *
     * @return mixed
     */
    protected function getProtectedProperty($object, $property)
    {
        $class = new \ReflectionClass(get_class($object));

        $property = $class->getProperty($property);
        $property->setAccessible(true);

        return $property->getValue($object);
    }

    protected function getFixturesDir()
    {
        return __DIR__ . '/../fixtures/';
    }

    protected function transformData($data = [])
    {
        $transformedData = [];
        foreach ($data as $key => $value) {
            $snakeCaseKey = ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $key)), '_');
            $transformedData[$snakeCaseKey] = $value;
        }

        return $transformedData;
    }
}
